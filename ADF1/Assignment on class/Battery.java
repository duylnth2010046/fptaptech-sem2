/**
 * Write a description of class Battery here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Battery{
    /**
     * Fileds
     */
    private int energy;
    /**
     * Constructor for object of class Battery
     */
    public Battery(){
        //To do:
        energy=100;
    }
    /**
     * Method
     */
    public void setEnergy(int value) {
        energy=value;
    }
    public int getEnergy(){
        return energy;
    }
    public void decreaseEnergy(){
        energy--;
    }
}