/**
 * Write a descreption of class FlashLamp here
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class FlashLamp{
    /**
     *Fileds
     */
    private  boolean status;
    private Battery battery;
    //"Dat object Battery vao FlashLamp van chay tot -> LTHDT"
    /**
     * Constructor for objects of class FlashLamp
     */
    public FlashLamp(){
        //To do:
        status=false;
    }
    /**
     * Methods
     */
    public void setBattery(Battery battery){
        this.battery=battery;
    }
    public int getBatteryInfo() {
        return battery.getEnergy();
    }
    public void light(){
        if(status==true&&battery!=null&&battery.getEnergy()>0){
            battery.decreaseEnergy();
        }
    }
    public void turnOn(){
        if(battery!=null&&battery.getEnergy()>0){
            status=true;
        }
        light();
    }
    public void turnOff(){
        status=false;
    }
}