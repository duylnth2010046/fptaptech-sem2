/**
 * Lop client su dung lop Student de xay dung chuong tring java theo kcih ban
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Client {
    public static void main(String []args){
        Student studentA;
        Student studentB;

        studentA=new Student();
        studentB=new Student(1,"Nguyen Van A", true);

        studentB.printInfo();

        studentB.setName("Nguyen Van B");
        studentB.printInfo();
    }
}