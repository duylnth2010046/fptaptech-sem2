package Assignments.assignments.Assignment12;

import java.util.Scanner;

public abstract class Account {

    protected String customerName;
    protected String accountNumber;
    protected double balance;

   public Account(String customerName, String accountNumber, double balance){
       this.customerName = customerName;
       this.accountNumber = accountNumber;
       this.balance = balance;
   }
   

    public void setCustName(String customerName){
        Scanner input = new Scanner(System.in);
        System.out.print("\nEnter customer name :");
        customerName = input.next();
    }
    public String getCustName(){
        return customerName;
    }

     
     public void setAccNumber(String accountNumber){
        Scanner input = new Scanner(System.in);
        System.out.print("\nEnter account number :");
        accountNumber = input.next();
    }
    public String getAccNumber(){
        return accountNumber;
    }


    public void setBalance(double balance){
        Scanner input = new Scanner(System.in);
        System.out.print("\nEnter balance :");
        balance = input.nextDouble();
    }
    public double getBalance(){
        return balance;
    }
    
    
}
