package Assignments.assignments.Assignment12;

import java.util.Scanner;

public class BankTest extends Bank{// Loi ko the ke thua
    
    public static void main(String[] args ){

            Bank bank = new Bank();
        Scanner scanner = new Scanner(System.in);
        while(true){

            System.out.println(" Elvis Bank system");
            System.out.println(" Enter 1 : To create account");
            System.out.println(" Enter 2 : To display account details");
            System.out.println(" Enter 3 : To withdraw money from your account");
            System.out.println(" Enter 4 : To deposit money");
            System.out.println(" Enter 5 : To exit");
            int choice = scanner.nextInt();

            switch(choice){
                case 1:
                // Tao TK
                bank.createAccount();
                break;

                case 2:
                // Van tin TK
                bank.displayAccountDetails();
                break;

                case 3:
                // Rut tien
                bank.withdraw(12);//tam thoi
                break;

                case 4:
                // Gui tien
                bank.deposit(34);
                break;

                case 5:
                System.out.println("\nThank you ! See you again !");
                return;

                default:
                System.out.println(" Invalided choice !");
                 continue;

            }

        }
    }
    
}
