package com.example.java11;
import com.sun.org.apache.xpath.internal.objects.XString;
import org.apache.axis.encoding.Base64;

import java.io.UnsupportedEncodingException;

public class Base64EncodeDecode {

    public static String encodeData(String text)
        throws UnsupportedEncodingException {
        byte[] bytes = text.getBytes("UTF-8");

        String encodString = Base64.encode(bytes);
        return encodString;
    }
    public static String decodeData(String encodeText)
        throws UnsupportedEncodingException {
        byte[] decodeBytes = Base64.decode(encodeText);
        String string = new String(decodeBytes,"UTF-8");
        return string;

    }

    public static void main(String[] args)
        throws UnsupportedEncodingException {

        String text ="Java change your life";
        //Ma hoa
        String encodeText = encodeData(text);
        System.out.println(encodeText);
        //Giai ma
        String decodeText = decodeData(encodeText);
        System.out.println(decodeText);






    }
}



Maven Repository: org.apache.axis » axis » 1.4
https://mvnrepository.com


