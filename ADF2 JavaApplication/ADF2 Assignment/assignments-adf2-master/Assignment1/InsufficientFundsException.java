package Assignment1;

public class InsufficientFundsException extends Exception{

    public InsufficientFundsException(){
        super();
    }

    public InsufficientFundsException(double available){
        super("The withdraw " +available +" is available !");
    }
}
