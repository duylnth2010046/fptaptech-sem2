package BT_Lab;
import java.io.*;
import java.util.*;
public class Application {

    public Application(){

    }

    public static void main(String[] args) {
        Student student = new Student();
        Scanner input = new Scanner(System.in);
        Scanner scanner = new Scanner(System.in);

        while(true){
            System.out.println("\n ------- MENU ----------");
            System.out.println(" Enter 1 : To set Student's information and save to file");
            System.out.println(" Enter 2 : To display Student's information.");
            System.out.println(" Enter 3 : To exit .");
            int choice = scanner.nextInt();

            switch(choice){
                case 1:
                    student.setInformation();
                    student.saveState();
                    break;

                case 2:
                    student.display();
                    break;

                case 3:
                    System.out.println("\nThank you ! See you again !");
                    return;

                default:
                    System.out.println(" Invalided choice !");
                    continue;

            }

        }
    }
}
