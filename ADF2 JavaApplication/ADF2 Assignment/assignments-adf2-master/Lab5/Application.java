package Lab5;
import java.io.*;
import java.util.*;
public class Application {

    public Application(){

    }

    public static void main(String[] args) {
        ArrayList<Student> studentList = new ArrayList<Student>();
         Student student = new Student();
        Scanner input = new Scanner(System.in);
        Scanner scanner = new Scanner(System.in);

        while(true){
            System.out.println("\n ------- MENU ----------");
            System.out.println(" Enter 1 : To add Students ");
            System.out.println(" Enter 2 : To update a student");
            System.out.println(" Enter 3 : To delete a student .");
            System.out.println(" Enter 4 : To search students .");
            System.out.println(" Enter 5 : To display all students .");
            System.out.println(" Enter 6 : To save to file .");
            System.out.println(" Enter 7 : To load from file .");
            System.out.println(" Enter 8 : To exit .");
            int choice = scanner.nextInt();

            switch(choice){
                case 1:
                    student.setRollNumber();
                    student.setName();
                    student.setAge();
                    student.setMark();

                    break;
                case 2:
                    student.update();//error
                    break;
                case 3:
                    student.delete();//error
                    break;
                case 4://error
                    student.search();
                    break;
                case 5:
                    student.display();
                    break;
                case 6:
                    student.saveState();
                    break;
                case 7:
                    student.displayInFile();
                    break;
                case 8:
                    System.out.println("\nThank you ! See you again !");
                    return;
                default:
                    System.out.println(" Invalided choice !");
                    continue;

            }
        }
    }
}
