package Lab5;
import java.util.*;
public class LyricSet {

    private static final String[] lyric = {"you" ,"say", "it" ,"best", "when" , "you","say","nothing","at","all"};

    public static void main(String[] args) {
        // Create a set from HashSet that's implemented Set
        Set words = new HashSet();

        //Add all words to Set
        for (String w : lyric)
            words.add(w);

        //Traverse the list
        // NOTICE : set doesn't allow duplicate items - ko trung lap
        for (Object o :words)
            System.out.print( o +" ");
        System.out.println("\n--------------------------");
        System.out.println("Contains [you] ?" +words.contains("you"));
        System.out.println("Contains [me] ?:" +words.contains("me"));
    }
}
/*
    Perform following steps and answer these questions:
       1.	Compile , run file and observe result, compare with LyricList example above.
          Answer : Different :Hash Set doesn't return the duplicate item
                              => There is no indexOf() methods in HashSet
                   Similar : Both  methods has add(),contain() ,... methods
       2.	Replace HashSet by LinkedHashSet or TreeSet. Find the differences.
       3.	Note that Set is unordered, find the way to get an index an element in Set.
            We could set an index for the String then add items to Set

 */