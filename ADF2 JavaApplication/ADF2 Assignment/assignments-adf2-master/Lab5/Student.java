package Lab5;
import java.util.*;
import java.io.*;
public class Student {

    private String rollNumber;
    private String name;
    private int age;
    private double mark;

    public Student(){

    }

    public Student(String rollNumber, String name, int age, double mark){
        this.rollNumber = rollNumber;
        this.name = name;
        this.age = age;
        this.mark = mark;
    }

    public void setName(){

        Scanner input = new Scanner(System.in);

        System.out.println("Enter Student name :");
        name = input.nextLine();


        this.saveState();
        System.out.println("\n --------- Add new student's information DONE !");
    }

    public void setRollNumber(){
        Scanner input = new Scanner(System.in);
        System.out.println("Enter Student roll number :");
        rollNumber = input.nextLine();
        this.saveState();
    }
    public void setAge(){
        Scanner input = new Scanner(System.in);
        do {
            System.out.println("Enter Student age:");
            age = input.nextInt();
            if(age <=0 ){
                System.out.println("\nStudent's age is invalid. ");
            }
        }while(age <= 0);
        this.saveState();
    }
    public void setMark(){
        Scanner input = new Scanner(System.in);
        do {
            System.out.println("Enter Student mark :");
            mark = input.nextDouble();
            if(mark <0){
                System.out.println("\nStudent's mark is invalid .");
            }
        }while(mark <0 );
        this.saveState();
        System.out.println("\n --------- Add new student's information DONE !");
    }

    public void saveState(){
        try{
            File file = new File("D:\\epc\\Ap-Sem2\\ADF2\\Collection\\src\\Lab5\\Student.txt");
            if (!file.exists()){
                file.createNewFile();
            }
            FileWriter fileWriter = new FileWriter(file,true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(" - " + this.name);
            bufferedWriter.write("\t { ");
            bufferedWriter.write(" Roll number : "+this.rollNumber );
            bufferedWriter.write("  |  Age :" +Integer.toString(this.age));
            bufferedWriter.write("  |  Mark : "+Double.toString(this.mark));
            bufferedWriter.write("\t");
            bufferedWriter.newLine();
            bufferedWriter.close();
            fileWriter.close();

            System.out.println("\n  ----- Save to file successfully !");
        }catch(FileNotFoundException fn){
            fn.printStackTrace();
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public void display(){
        System.out.println(" - " + this.name);
        System.out.println("\t { ");
        System.out.println(" Roll number : "+ this.rollNumber);
        System.out.println("  |  Age :" +Integer.toString(this.age));
        System.out.println("  |  Mark :" +Double.toString(this.mark) + " }");

    }
    public void displayInFile(){
        try{
            FileReader fileReader = new FileReader("D:\\epc\\Ap-Sem2\\ADF2\\Collection\\src\\Lab5\\Student.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            fileReader.close();
            System.out.println("\n --------- Please open file to view .");
        }catch (FileNotFoundException fn){
            fn.printStackTrace();
        } catch ( Exception ex){
            ex.printStackTrace();
        }
    }

    public void delete(){
        ArrayList<Student> arr = new ArrayList<Student>();
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter roll number need to be deleted :");
        String delete = sc.nextLine();
        int check = 0;
        for(Student st : arr){
            if(st.rollNumber.equals(delete)){
                arr.remove(st);
                System.out.println("Deleted successfully !" + delete);
                check++;
            }
        }
        if(check ==0){
            System.out.println("Roll number is not existed !");
        }

    }

    public void search(){
        ArrayList<Student> arr = new ArrayList<Student>();
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Roll number you want to search :");
        String rn = sc.nextLine();
        int check = 0;
        System.out.println("\nInformation of Student :");
        for ( Student st : arr){
            if(st.rollNumber.equals(rn)){
                st.display();
                check++;
            }
        }
        if (check == 0){
            System.out.println("Roll number is invalid !");
        }
    }

    public void update(){
        ArrayList<Student> arr = new ArrayList<Student>();
        Scanner sc = new Scanner (System.in);
        System.out.println("\nEnter Roll number to update :");
        String studentCode = sc.nextLine();

        for(Student st : arr){
            if (st.rollNumber.equals(studentCode)){
                System.out.println("\n INFORMATION COULD BE  UPDATED :");
                System.out.println("1. Student name ");
                System.out.println("2. Student age ");
                System.out.println("3. Student mark ");

               int choice = Integer.parseInt(sc.nextLine());
               if(choice == 1){
                   System.out.println("Update Student name :");
                   st.setName();
               }else if(choice == 2){
                   System.out.println("Update Student age :");
                   st.setAge();
               }else if(choice == 3){
                   System.out.println("Update Student mark :");
                   st.setMark();
               }else{
                   System.out.println("Invalid choice");
               }
                arr.set(arr.indexOf(st),st);
            }else {
                System.out.println("Invalid Roll number !");

        }
        }
    }

}
