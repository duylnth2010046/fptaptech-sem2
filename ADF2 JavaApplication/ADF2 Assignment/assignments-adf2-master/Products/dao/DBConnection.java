package Products.dao;

import java.sql.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBConnection {

    public static Connection createConnection(){
        String dbUrl = "jdbc:sqlserver://localhost:1433;databaseName=Product";
        Connection conn = null;
        try{
            conn = DriverManager.getConnection(dbUrl,"sa","sa");

        }catch (SQLException ex){
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE,null,ex);
        }
        return conn;
    }
}
