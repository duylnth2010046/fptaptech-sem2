package StudentSystem.db;
import StudentSystem.entity.Student;
import jdbcrework.entity.Person;

import java.sql.*;
import java.util.*;
public class StudentAccessor {

    public void insert(Student student) throws Exception{
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try{
            Connector connector = new Connector();
            connection = connector.getConnection();
            preparedStatement = connection.prepareStatement("Insert into Student values (?,?,?,?)");
            preparedStatement.setString(1,student.getId());
            preparedStatement.setString(2,student.getName());
            preparedStatement.setString(3,student.getAddress());
            preparedStatement.setString(4,student.getEmail());
            preparedStatement.executeUpdate();
        }finally {
            Connector.close(preparedStatement);
            Connector.close(connection);
        }
    }

    public void delete(String studentId) throws Exception{
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try{
            Connector connector = new Connector();
            connection = connector.getConnection();
            preparedStatement = connection.prepareStatement("delete from Student where StudentId = ?");
            preparedStatement.setString(1,studentId);
            preparedStatement.executeUpdate();
        }finally {
            Connector.close(preparedStatement);
            Connector.close(connection);
        }
    }

    public void update(Student student) throws  Exception{
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try{
            Connector connector = new Connector();
            connection = connector.getConnection();
            preparedStatement = connection.prepareStatement("update Student set StudentName = ? and Address = ? and Email = ? where StudentId = ?");
            preparedStatement.setString(1,student.getName());
            preparedStatement.setString(2,student.getId());
            preparedStatement.setString(3,student.getAddress());
            preparedStatement.setString(4,student.getEmail());
            preparedStatement.executeUpdate();
        }finally {
            Connector.close(preparedStatement);
            Connector.close(connection);
        }
    }

    public Student findById(String studentId) throws  Exception{
        Student reValue = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try{
            Connector connector = new Connector();
            connection = connector.getConnection();
            preparedStatement = connection.prepareStatement("select * from Student where StudentId = ?");
            preparedStatement.setString(1,studentId);
            resultSet = preparedStatement.executeQuery();

            if(resultSet.next()){
                String dbName = resultSet.getString(2);
                String dbAddress = resultSet.getString(3);
                String dbEmail = resultSet.getString(4);
                reValue = new Student();
                reValue.setId(studentId);
                reValue.setName(dbName);
                reValue.setAddress(dbAddress);
                reValue.setEmail(dbEmail);
            }
        }finally {
            Connector.close(resultSet);
            Connector.close(preparedStatement);
            Connector.close(connection);
        }
        return  reValue;
    }

    public List<Student> getStudents(String studentName) throws Exception{
        List<Student> reValue = new ArrayList<Student>();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet  = null;
        try{
            Connector connector = new Connector();
            connection = connector.getConnection();
            String sql = " Select * from Student";
            if(studentName != null && !studentName.equals("")){
                sql += " where StudentName like '" + studentName+"'";
            }
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                String studentId = resultSet.getString(1);
                String dbName = resultSet.getString(2);
                String dbAddress = resultSet.getString(3);
                String dbEmail = resultSet.getString(4);
                Student student = new Student();
                student.setId(studentId);
                student.setName(dbName);
                student.setAddress(dbAddress);
                student.setEmail(dbEmail);
                reValue.add(student);
            }
        }finally {
            Connector.close(resultSet);
            Connector.close(preparedStatement);
            Connector.close(connection);
        }
        return  reValue;
    }

}
