

public class RoomsNoAvailableException extends Exception {

    public RoomsNoAvailableException(){
          super();
    }

    public RoomsNoAvailableException(int roomsAvailable){
        super("Currently there are " + roomsAvailable + "rooms available");
    }
    
}
