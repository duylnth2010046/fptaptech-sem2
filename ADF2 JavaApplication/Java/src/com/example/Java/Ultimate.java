package com.example.Java;

import java.util.LinkedList;
import java.util.Queue;

public class Ultimate {
    public static void main(String[] args) {
        Queue<String> queue = new LinkedList<>();
        queue.add("SAMSUNG");
        queue.add("SONY");
        queue.add("HTC");
        queue.add("APPLE");
        queue.add("VINFAST");
        while(true) {
            String value = queue.poll();
            if(value==null) break;
            System.out.println(value);
        }

    }
}
