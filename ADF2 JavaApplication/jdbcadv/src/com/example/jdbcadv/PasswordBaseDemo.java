package com.example.jdbcadv;

import sun.misc.BASE64Encoder;

import javax.crypto.Cipher;
import javax.crypto.EncryptedPrivateKeyInfo;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;

public class PasswordBaseDemo {
    static final String ALGO = "PBEWithMD5AndDes";
    public static void main(String[] args) {
        //1.Base on password
        //2.Created key based on keyspec by password
        //3.Created Salte
        //4.Parameter for decrypt/encrypt mode

        String password = "khong co password dau dung tim";
        SecretKeyFactory skf = SecretKeyFactory.getInstance(ALGO);
        PBEKeySpec keySpec = new PBEKeySpec(password.toCharArray());
        SecretKey secrecomtKey = skf.generateSecret(keySpec);
        SecureRandom random =   SecureRandom.getInstance("SHA1PRNG");
        byte [] salte = random .generateSeed(8);
        PBEParameterSpec param = new PBEParameterSpec(salte, 8);
        Cipher cipher = Cipher.getInstance(ALGO);


        //Encrypt
        cipher.init(Cipher.ENCRYPT_MODE,secretKey,param);
        String clear = "JAVA CHANGE YOUR LIFE";
        byte[] encrypted = cipher.doFinal(clear.getBytes());
        BASE64Encoder encoder = new BASE64Encoder();
        String encode = encoder.encode(encrypted);
        System.out.println("Encoded: "+ encode);


        //Decrypt
        cipher.init(Cipher.ENCRYPT_MODE,secretKey,param);
        byte[] decrypted = cipher.doFinal(encrypted);
        System.out.println("Decrypted: "+ new String(decrypted));

        System.out.println("Spend: "+(System.currentTimeMillis() -  cur));

    }
}
