﻿using System;

namespace Assignment_1
{
    class SystemChange
    {
        static void Main(string[] args)
        {
            const int percentConst = 40;
            string studentName;
            int age;
            string gender;
            string branch = "Computer Science";

            Console.Write("Enter Student Name : ");
            studentName = Console.ReadLine();

            Console.Write("Enter Student Age :");
            age = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter gender : ");
            gender = Console.ReadLine();

            Console.WriteLine();
            Console.WriteLine(" ==== Your details ====\n");
            Console.WriteLine("Name :" + studentName);
            Console.WriteLine("Age :" + age);
            Console.WriteLine("Gender :" + gender);
            Console.WriteLine("Branch :" + branch);

        }
    }
}
