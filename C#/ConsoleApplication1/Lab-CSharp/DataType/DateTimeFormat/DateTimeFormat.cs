﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateTimeFormat
{
    class DateTimeFormat
    {
        static void Main(string[] args)
        {
            DateTime dt = DateTime.Now; // obtain current time 

            Console.WriteLine("d format: {0:d}", dt);//d format: 8/10/2021
            Console.WriteLine("D format: {0:D}", dt);//D format: Tuesday, August 10, 2021

            Console.WriteLine("t format: {0:t}", dt);//t format: 3:36 PM
            Console.WriteLine("T format: {0:T}", dt);//T format: 3:36:41 PM

            Console.WriteLine("f format: {0:f}", dt);//f format: Tuesday, August 10, 2021 3:36 PM
            Console.WriteLine("F format: {0:F}", dt);//F format: Tuesday, August 10, 2021 3:36:41 PM

            Console.WriteLine("g format: {0:g}", dt);//g format: 8/10/2021 3:36 PM
            Console.WriteLine("G format: {0:G}", dt);//G format: 8/10/2021 3:36:41 PM

            Console.WriteLine("m format: {0:m}", dt);//m format: August 10
            Console.WriteLine("M format: {0:M}", dt);//M format: August 10

            Console.WriteLine("r format: {0:r}", dt);//r format: Tue, 10 Aug 2021 15:36:41 GMT
            Console.WriteLine("R format: {0:R}", dt);//R format: Tue, 10 Aug 2021 15:36:41 GMT
            Console.WriteLine("s format: {0:s}", dt);//s format: 2021-08-10T15:36:41

            Console.WriteLine("u format: {0:u}", dt);//u format: 2021-08-10 15:36:41Z
            Console.WriteLine("U format: {0:U}", dt);//U format: Tuesday, August 10, 2021 8:36:41 AM

            Console.WriteLine("y format: {0:y}", dt);//y format: August 2021
            Console.WriteLine("Y format: {0:Y}", dt);// Y format: August 2021

        }
    }
}
