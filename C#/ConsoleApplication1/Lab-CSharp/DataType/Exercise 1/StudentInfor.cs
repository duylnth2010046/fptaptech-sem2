﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_1
{
    class StudentDetails
    {
        public string name;
        public string address;
        public string phone;
    }
    class StudentInfor
    {
        static void Main(string[] args)
        {
            StudentDetails stdt = new StudentDetails();
            Enter(stdt);
            Display(stdt);
        }

        static void Enter(StudentDetails stdt)
        {
            Console.WriteLine("Enter Student Name :");
            stdt.name = Console.ReadLine();

            Console.WriteLine("Enter Student Address :");
            stdt.address = Console.ReadLine();

            Console.WriteLine("Enter Student Phone Number :");
            stdt.phone = Console.ReadLine();
        }

        static void Display(StudentDetails stdt)
        {
            Console.WriteLine("\n");
            Console.WriteLine("==== Student Detail ====");
            Console.WriteLine("Name : " + stdt.name.ToUpper());
            Console.WriteLine("Address : " + stdt.address);
            Console.WriteLine("Phone number : " + stdt.phone);
        }
    }
}
