﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;  

namespace Lab_2_Menu
{
    class TeamManagement
    {
        private List<Team> listTeam = null;
        public TeamManagement()
        {
            listTeam = new List<Team>(); 
        }

        // Ham tao STT tang dan cho doi bong
        private int GenerateID()
        {
            int max = 1;
            if( listTeam != null && listTeam.Count >0)
            {
                max = listTeam[0].STT;
                foreach (Team t in listTeam)
                {
                    if (max < t.STT)
                    {
                        max = t.STT;
                    }
                }
                max++;
            }
            return max;
        }

        //Ham dem so doi bong
        public int countTeam()
        {
            int count = 0;
            if(listTeam != null)
            {
                count = listTeam.Count;
            }
            return count;
        }

        //Ham them moi doi bong
        public void createTeam()
        {
            Team team = new Team();
            team.STT = GenerateID();
            Console.WriteLine("Nhập mã đội :");
            team.TeamID = Console.ReadLine();

            Console.WriteLine("Nhập tên đội :");
            team.TeamName = Console.ReadLine();

            Console.WriteLine("Nhập tên Huấn luyện viên :");
            team.Coach = Console.ReadLine();

            listTeam.Add(team);
        }

        public void updateTeam(string ID)
        {
            Team team = FindByID(ID);
            if(team != null)
            {
                Console.WriteLine("Nhap ten doi bong :");
                string name = Convert.ToString(Console.ReadLine());
                //neu ko nhap j thi ten ko doi
                if(name!=null && name.Length > 0)
                {
                    team.TeamName = name;
                }

                Console.WriteLine("Nhap ten huan luyen vien:");
                string coach = Convert.ToString(Console.ReadLine());
                if (coach != null && coach.Length > 0)
                {
                    team.Coach = coach;
                }

            }
            else
            {
                Console.WriteLine("\nDoi bong co ID = {0} khong ton tai !", ID);
            }
        }

        //Ham tim kiem doi bong theo ID
        public List<Team> FindByID( string ID)
        {
            List<Team> searchID = new List<Team>();
            if(listTeam != null && listTeam.Count > 0)
            {
                foreach(Team team in listTeam)
                {
                    if (team.TeamName.ToUpper().Contains(ID.ToUpper()))
                    {
                        searchID.Add(team);
                    }
                }
            }
            return searchID;
        }

    }
}
