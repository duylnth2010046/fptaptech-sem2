﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_2_Menu
{
    class TeamDetails
    {
        public string teamID;
        public string teamName;
        public string coachName;
    }

    class Program
    {

        static void printLine(int n)
        {
            Console.ReadLine();
            for(int i = 0; i < n; i++)
            {
                Console.Write("_");
            }
            Console.ReadLine();
        }

        static void menuMain()
        {
            
            int key;
            while (true)
            {
                Console.WriteLine("\n\n\t\t\t\t HỆ THỐNG V-LEAGUE 2021 \n  ");
                Console.WriteLine("\t\t================== MAIN MENU ===================");
                Console.WriteLine("\t\t|     1. Quản lý danh sách đội bóng            |");
                Console.WriteLine("\t\t|     2. Quản lý lịch thi đấu                  |");
                Console.WriteLine("\t\t|     3. Quản lý kết quả thi đấu               |");
                Console.WriteLine("\t\t|     4. Thống kê                              |");
                Console.WriteLine("\t\t|     0. Thoát                                 |");
                Console.WriteLine("\t\t================================================");
                Console.Write("Nhập lựa chọn : ");
                key = Convert.ToInt32(Console.ReadLine());
                switch (key)
                {
                    case 1:
                        Console.Clear();
                        menuQuanLyDS();
                        Console.Clear();
                        break;
                    case 2:
                        Console.Clear();
                        menuSchedule();
                        Console.Clear();
                        break;
                    case 3:
                        Console.Clear();
                        menuResult();
                        Console.Clear();
                        break;
                    case 4:

                        Console.Clear();
                        break;
                    case 0:
                        Console.WriteLine("\n\n\t\t \u263A \u263A \u263A Goodbye !! \u263A \u263A \u263A\n\n");
                        Environment.Exit(0);
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("\n\n\t\t\tChức năng không hợp lệ | Mời nhập lại !\n\n");
                        break;
                }
            }
        }
        static void Main(string[] args)
        {
            Console.InputEncoding = Encoding.UTF8;
            Console.OutputEncoding = Encoding.UTF8;
            menuMain();
           
        }

       
        //Menu quản lý danh sách đội bóng
        static void menuQuanLyDS()
        {
            TeamDetails[] td = new TeamDetails[1000];
            int key;
            int idCount = 0;
            int numTeam = 0;


            while (true)
            {
                Console.WriteLine("\t\t\t Quản lý đội bóng");

                Console.WriteLine("\t\t============================================");
                Console.WriteLine("\t\t=    1. Xem danh sách đội bóng             =");
                Console.WriteLine("\t\t=    2. Cập nhật danh sách đội bóng        =");
                Console.WriteLine("\t\t=    3. Thêm mới một đội bóng              =");
                Console.WriteLine("\t\t=    4. Xóa một đội bóng                   =");
                Console.WriteLine("\t\t=    5. Xem danh sách theo thứ tự mã đội   =");
                Console.WriteLine("\t\t=    6. Xem danh sách theo tên đội         =");
                Console.WriteLine("\t\t=    0. Trở về Menu chính                  =");
                Console.WriteLine("\t\t============================================");
                Console.Write("Nhập lựa chọn : ");
                key = Convert.ToInt32(Console.ReadLine());
                switch (key)
                {
                    case 1:
                        Console.WriteLine("\n1. ");

                        Console.Clear();
                        break;
                    case 2:

                        Console.Clear();
                        break;
                    case 3:
                        Console.WriteLine("\n3. Thêm mới đội bóng ");
                          addTeam(td,"hi",numTeam);
                        Console.Clear();
                        break;
                    case 4:

                        Console.Clear();
                        break;
                    case 5:

                        Console.Clear();
                        break;
                    case 6:


                        Console.Clear();
                        break;

                    case 0:
                        Console.Clear();
                        menuMain();
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("\n\n\t\t\tChức năng không hợp lệ | Mời nhập lại !\n\n");
                        break;
                }
            }
        }

        static void addTeamDetail(TeamDetails td, string id)
        {
            Console.WriteLine("Nhập mã đội :");
            td.teamID = Console.ReadLine();
            Console.WriteLine("Nhập tên đội :");
            td.teamName = Console.ReadLine();
            Console.WriteLine("Nhập tên Huấn luyện viên :");
            td.coachName = Console.ReadLine();

            td.teamID = id;
        }
        static void addTeam(TeamDetails[] a,string id, int n)
        {
            printLine(40);
            Console.WriteLine("\n Nhập đội thứ {0}", n + 1);
            addTeamDetail(a[100], id);
            printLine(40);
        }

        static void updateTeamDetail(TeamDetails td)
        {
            Console.WriteLine("Nhập tên đội :");
            td.teamName = Console.ReadLine();

            Console.WriteLine("Nhập tên Huấn luyện viên :");
            td.coachName = Console.ReadLine();
        }
        static void updateTeam(TeamDetails[] a, string id, int n)
        {
            int found = 0;
            for(int i = 0; i < n; i++)
            {
                if (a[i].teamID == id)
                {
                    found = 1;
                    printLine(40);
                    Console.WriteLine("\nCap nhat thong tin cua doi co ID = "+ id);
                    updateTeamDetail(a[i]);
                    printLine(40);
                    break;
                }
            }
            if (found == 0)
            {
                Console.WriteLine("\n Doi bong co ID {0} khong ton tai !" , id);
            }
        }
       
        // Menu quản lý lịch thi đấu
        static void menuSchedule()
        {
            TeamDetails td = new TeamDetails();
            int key;
            int idCount = 0;

            while (true)
            {
                Console.WriteLine("\t\t\t Quản lý lịch thi đấu");
                Console.WriteLine("\t\t============================================");
                Console.WriteLine("\t\t=    1. Xem lịch thi đấu                   =");
                Console.WriteLine("\t\t=    2. Cập nhật lịch thi đấu              =");
                Console.WriteLine("\t\t=    3. Thêm mới lịch thi đấu              =");
                Console.WriteLine("\t\t=    4. Xóa lịch thi đấu                   =");
                Console.WriteLine("\t\t=    0. Trở về Menu chính                  =");
                Console.WriteLine("\t\t============================================");
                Console.Write("Nhập lựa chọn : ");
                key = Convert.ToInt32(Console.ReadLine());
                switch (key)
                {
                    case 1:

                        Console.WriteLine("\n1. ");

                        Console.Clear();
                        break;
                    case 2:

                        Console.Clear();
                        break;
                    case 3:
                        Console.WriteLine("\n3. Them moi doi bong ");

                        Console.Clear();
                        break;
                    case 4:

                        Console.Clear();
                        break;
                   
                    case 0:
                        Console.Clear();
                        menuMain();
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("\n\n\t\t\tChức năng không hợp lệ | Mời nhập lại !\n\n");
                        break;

                }
            }
        }

        static void menuResult()
        {
            TeamDetails td = new TeamDetails();
            int key;
            int idCount = 0;

            while (true)
            {
                Console.WriteLine("\t\t\t Kết quả thi đấu");
                Console.WriteLine("\t\t============================================");
                Console.WriteLine("\t\t=    1. Xem lịch thi đấu                   =");
                Console.WriteLine("\t\t=    2. Cập nhật lịch thi đấu              =");
                Console.WriteLine("\t\t=    3. Thêm mới lịch thi đấu              =");
                Console.WriteLine("\t\t=    4. Xóa lịch thi đấu                   =");
                Console.WriteLine("\t\t=    0. Trở về Menu chính                  =");
                Console.WriteLine("\t\t============================================");
                Console.Write("Nhập lựa chọn : ");
                key = Convert.ToInt32(Console.ReadLine());
                switch (key)
                {
                    case 1:

                        Console.WriteLine("\n1. ");

                        Console.Clear();
                        break;
                    case 2:

                        Console.Clear();
                        break;
                    case 3:
                        Console.WriteLine("\n3. Them moi doi bong ");

                        Console.Clear();
                        break;
                    case 4:

                        Console.Clear();
                        break;

                    case 0:
                        Console.Clear();
                        menuMain();
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("\n\n\t\t\tChức năng không hợp lệ | Mời nhập lại !\n\n");
                        break;

                }
            }
        }

    }
}
