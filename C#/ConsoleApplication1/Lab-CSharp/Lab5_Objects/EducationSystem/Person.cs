﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationSystem
{
   public abstract class Person
    {
        protected string pName;
        protected string phoneNo;
        protected string email;

        protected Person()
        {
            pName = "me";
            phoneNo = "02345-234-343";
            email = "ohimark.com";
        }
        protected Person(string name,string phone,string email)
        {
            this.pName = name;
            this.phoneNo = phone;
            this.email = email;
        }

        public string PName
        {
            get { return pName; }
            set { pName = value; }
        }
        public string PPhoneNo
        {
            get { return phoneNo; }
            set { phoneNo = value; }
        }

        public string PEmail
        {
            get { return email; }
            set { email = value; }
        }


    }
}
