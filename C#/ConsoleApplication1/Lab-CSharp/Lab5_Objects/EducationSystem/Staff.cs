﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EducationSystem
{
        class Staff : Employee
        {
            private string title;    
            public Staff(string t)
            {
                this.title = t;
            }

            public Staff(string t, string department, double salary, string date, string name, string phone, string email) : base(department, salary, date, name, phone, email)
            {
               this.title = t;
            }

            public override string ToString()
            {
                return "\n\t Student : \n Name : " + pName + "\n Phone Number : " + phoneNo +
                    "\n Email : " + email + "\n Department : " + department + "\n Salary : " + salary + " $" + "\n Date Hired : " + dateHired
                    + "\n Title :" + title ;
            }

        public override double CalculateBonus()
        {
            return salary * 0.06;
        }
        public override double CalculateVacation()
        {
            int year = Convert.ToInt32(DateTime.Now.Year - Convert.ToInt32(dateHired));
            if (year >= 5)
            {
                return 4;
            }
            else
            {
                return 3;
            }
        }

    }    
}
