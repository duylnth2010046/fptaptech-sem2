﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5_Objects
{
   public abstract class GeometricObjects
    {
        protected string color;
        protected double weight;

        protected GeometricObjects()
        {
            color = "white";
            weight = 1.0;
        }

        protected GeometricObjects(string color, double weight)
        {
            this.color = color;
            this.weight = weight;
        }

        public string PColor
        {
            get { return color; }
            set { color = value; }
        }
        public double PWeight
        {
            get { return weight; }
            set { weight = value; }
        }
        public abstract double findArea();
        public abstract double findPerimeter();
    }
}
