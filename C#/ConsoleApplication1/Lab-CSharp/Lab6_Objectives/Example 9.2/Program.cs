﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example_9._2
{
    class Program
    {
        static void Main(string[] args)
        {
            string driverName = Console.ReadLine();
            Ferrari ferrari = new Ferrari(driverName);
            ferrari.PushGasPedal();
            ferrari.UserBrakes();
            Console.WriteLine(ferrari);
            Console.ReadLine();
        }
    }
}
