﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example08
{
    public class Student
    {
        private string name;
        private int age;
        private double grade;

        public double Grade
        {
            get { return grade; }
            set { grade = value; }
        }

        public int Age
        {
            get { return age; }
            set { age = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public Student() { }
        public Student(string name, int age, double grade)
        {
            this.Name = name;
            this.Age = age;
            this.Grade = grade;
        }
    }
    public class StudentSystem
    {
        private Dictionary<string, Student> repo;
        public Dictionary<string,Student> Repo
        {
            get { return repo; }
            private set { repo = value; }
        }
        public StudentSystem()
        {
            this.repo = new Dictionary<string, Student>();
        }
       public void ParseCommand()
        {
            string[] args = Console.ReadLine().Split();
            if (args[0] == "Create")
            {
                var name = args[1];
                var age = int.Parse(args[2]);
                var grade = double.Parse(args[3]);

                if(!repo.ContainsKey(name))//neu da ton tai
                {
                    var student = new Student(name, age, grade);
                    Repo[name] = student;
                }
                else
                {
                    Console.WriteLine("Da ton tai !!");
                }
            }else if(args[0] == "Show")
            {
                var name = args[1];
                if (Repo.ContainsKey(name))
                {
                    var student = Repo[name];
                    string view = $"{student.Name} is { student.Age} years old and grade is{student.Grade}";
                    Console.WriteLine(view);
                }
                
            }else if (args[0] == "exit")
            {
                Environment.Exit(0);
            }
        }
    }
}
