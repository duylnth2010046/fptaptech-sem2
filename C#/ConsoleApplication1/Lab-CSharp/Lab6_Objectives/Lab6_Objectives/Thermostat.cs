﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6_Objectives
{
    class Thermostat
    {
        public delegate void TemperatureChangeHandler(
            float newTemperature);

        private TemperatureChangeHandler _OnTemperatureChange;
        public TemperatureChangeHandler OnTeamperatureChange
        {
            get { return _OnTemperatureChange; }
            set { _OnTemperatureChange = value; }
        }

        public float CurrentTemperature
        {
            get { return _CurrentTemperature; }
            set
            {
                if(value != CurrentTemperature)
                {
                    _CurrentTemperature = value;
                    if (OnTeamperatureChange != null)
                    {
                        OnTeamperatureChange(value);
                    }
                }
            }
        }
        private float _CurrentTemperature;
    }  
}
