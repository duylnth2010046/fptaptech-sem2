﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoYourself_EmployeeSystem
{
    class EmployeeSystem
    {
        EmployeeManagement empManage = new EmployeeManagement();

        static void PressAnyKey()
        {
            Console.WriteLine("\n\t\tNhấn phím bất kì để tiếp tục...");
            Console.ReadLine();
            Console.Clear();
        }

        public void Main(string[] args)
        {
            Console.InputEncoding = Encoding.UTF8;
            Console.OutputEncoding = Encoding.UTF8;
            int key;
            string answer;
            while (true)
            {
                Console.WriteLine("\t\t ================== MAIN MENU ===================");
                Console.WriteLine("\t\t          1. Them nhan vien            ");
                Console.WriteLine("\t\t          2. Sua nhan vien                  " );
                Console.WriteLine("\t\t          3. Xoa nhan vien                  ");
                Console.WriteLine("\t\t          4. Hien thi danh sach nhan vien                  ");
                Console.WriteLine("\t\t          0. Thoát.                               ");
                Console.WriteLine("\t\t ================================================");
                try
                {
                    Console.Write("\n\t# Nhập lựa chọn : ");
                    key = Convert.ToInt32(Console.ReadLine());

                    switch (key)
                    {
                        case 1:
                            Console.WriteLine("\n\t\t| Thêm mới NV |\n");
                            empManage.CreateEmployee();
                            Console.Write("\t\tBạn có muốn tiếp tục không ? (Y/N) :");
                            answer = Convert.ToString(Console.ReadLine());
                            if (answer == "Y" || answer == "y")
                            {
                                Console.Clear();
                                goto case 1;
                            }
                            else if (answer == "N" || answer == "n")
                            {
                                Console.Clear();
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("\n\t\tBạn đã nhập sai => Tự động thoát chương trình !\n\n");
                            }
                            break;

                        case 2:
                            if (empManage.CountEmployee() > 0)
                            { 
                                Console.WriteLine("\n\t\t| Cập nhật NV |\n");
                                Console.Write("\nNhập ten NV : ");
                                string name = Console.ReadLine();
                                empManage.updateEmployee(name);
                            }
                            else
                            {
                                Console.WriteLine("\n\t\tDanh sách NV trống !\n\n");
                            }
                            Console.Write("\t\tBạn có muốn tiếp tục không ? (Y/N) :");
                            answer = Convert.ToString(Console.ReadLine());
                            if (answer == "Y" || answer == "y")
                            {
                                Console.Clear();
                                goto case 2;
                            }
                            else if (answer == "N" || answer == "n")
                            {
                                Console.Clear();
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("\n\t\tBạn đã nhập sai => Tự động thoát chương trình !\n\n");
                            }
                            break;

                        case 3:
                            if (empManage.CountEmployee() > 0)
                            {
                                string id;
                                Console.WriteLine("\n\t\t| Xóa NV |\n");
                                Console.Write("\nNhập ten NV : ");
                                id = Console.ReadLine();
                                if (empManage.DeleteByName(id))
                                {
                                    Console.WriteLine("\nNhan vien co name = {0} đã bị xóa \n\n", id);
                                }
                                else
                                {
                                    Console.WriteLine("\nNhan vien có thể đã bị xóa hoặc không tồn tại !\n\n");
                                }
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("\n\t\tDanh sách nhan vien trống !\n\n");
                            }
                            Console.Write("\t\tBạn có muốn tiếp tục không ? (Y/N) :");
                            answer = Convert.ToString(Console.ReadLine());
                            if (answer == "Y" || answer == "y")
                            {
                                Console.Clear();
                                goto case 3;
                            }
                            else if (answer == "N" || answer == "n")
                            {
                                Console.Clear();
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("\n\t\tBạn đã nhập sai => Tự động thoát chương trình !\n\n");
                            }
                            break;

                        case 4:
                            if (empManage.CountEmployee() > 0)
                            {
                                Console.WriteLine("\n\t\t| Xem danh sách NV |\n");
                                empManage.ShowEmployee(empManage.GetEmployees());
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("\n\t\t Danh sách câu hỏi trống ! \n\n");
                            }
                            PressAnyKey();
                            break;

                        case 0:
                            Console.WriteLine("\n\n\t\t \u263A \u263A \u263A Goodbye !! \u263A \u263A \u263A\n\n");
                            Environment.Exit(0);
                            break;


                        default:
                            Console.Clear();
                            Console.WriteLine("\n\n\t\t\tChức năng không hợp lệ | Mời nhập lại !\n\n");
                            break;


                    }
                }
                catch (Exception error)
                {
                    Console.Clear();
                    Console.WriteLine("\n\t\t\t Kiểu giá trị nhập vào không hợp lệ !!");
                    Console.WriteLine("\t\t\t=> " + error.Message);
                }

            }
        }
    }
}
