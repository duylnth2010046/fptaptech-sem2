﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_Assignment2_Group
{
    public class Exam:QuestionAnswer
    {
       
        public string fileName { get; set; }
        public int questionNo { get; set; }

        public int qaSTT { get; set; }
        public string qaCategoryID { get; set; }
        public string qaField { get; set; }
        public string qaQuestionID { get; set; }
        public string qaQuestionContent { get; set; }
        public double qaPoint { get; set; }
        public string qaAnswerA { get; set; }
        public string qaAnswerB { get; set; }
        public string qaAnswerC { get; set; }
        public string qaAnswerD { get; set; }
      
    }
}
