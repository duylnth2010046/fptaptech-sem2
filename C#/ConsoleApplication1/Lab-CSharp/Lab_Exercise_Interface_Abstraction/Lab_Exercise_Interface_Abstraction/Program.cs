﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_Exercise_Interface_Abstraction
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter radius : ");
            var radius = int.Parse(Console.ReadLine());
            IDrawShape circle = new Circle(radius);
            circle.Draw();

            Console.Write("Enter width : ");
            var width = double.Parse(Console.ReadLine());
            Console.Write("Enter height : ");
            var height = double.Parse(Console.ReadLine());
            IDrawShape rect = new Rectangle(width, height);
            rect.Draw();

        }
    }
}
