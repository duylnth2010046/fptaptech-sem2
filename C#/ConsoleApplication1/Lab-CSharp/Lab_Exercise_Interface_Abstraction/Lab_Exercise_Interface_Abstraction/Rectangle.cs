﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_Exercise_Interface_Abstraction
{
    public class Rectangle:IDrawShape
    {
        public double width { get; set; }
        public double height { get; set; }
        public Rectangle(double width, double height)
        {
            this.width = width;
            this.height = height;
        }
        public void Draw()
        {
           for(int i = 1; i <= height; i++)
            {
                for(int k = 1; k <= width; k++)
                {
                    if (i == 1 || k == 1 || i == height || k == width)
                    {
                        Console.Write("*");
                    }
                    else
                    {
                        Console.Write(" ");
                    }                 
                }
                Console.Write("\n");
            }
        }
       
    }
}
