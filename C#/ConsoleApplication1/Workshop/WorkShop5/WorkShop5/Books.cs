﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkShop5
{
    class Books
    {
        static void Main(string[] args)
        {
            /*
            string[] bookTitle = new string[6];
            for(int i = 0; i < bookTitle.Length; i++)
            {
                Console.Write("Enter the title of book : ");
                bookTitle[i] = Console.ReadLine();
                Console.WriteLine();
            }

            Console.WriteLine("List of Books : ");
            foreach(string title in bookTitle)
            {
                Console.WriteLine(title);
            }

            bookTitle[3] = "Planet of the Apes";
            Console.WriteLine("Newly changed title of book 4 : " + bookTitle[3]);
            */

            string[] colName = new string[4] { "Book Title", "Author", "Publisher", "Price" };

            string[,] bookDetails = new string[2, 4];

            Console.WriteLine("Enter book details : \n");
            for(int i = 0; i < bookDetails.GetLength(0); i++){
                for(int j = 0; j < 4; j++)
                {
                    Console.Write("{0} : ", colName[j]);
                    bookDetails[i, j] = Console.ReadLine();
                }
                Console.WriteLine();
            }

            Console.WriteLine("Details of books : \n");
            foreach(string names in colName)
            {
                Console.Write("{0}\t\t", names);
            }
            Console.WriteLine();

            for(int i = 0; i < bookDetails.GetLength(0); i++)
            {
                for(int j = 0; j < 4; j++)
                {
                    Console.Write(bookDetails[i, j] + "\t");
                }
                Console.WriteLine();
            }
        }
    }
}
