<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    //use HasFactory;
    public $fillable = [
        'furniture_name',
        'furniture_price',
        'furniture_avt',
    ];
}