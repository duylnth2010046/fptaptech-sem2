<?php

use Illuminate\Database\Seeder;
use App\Stock;

class StockTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Stock::create([
            'furniture_name' => 'Ghe oc cho',
            'furniture_price' => 8000000,
            'furniture_avt' => 'image.jpg',
        ]);
    }
}
